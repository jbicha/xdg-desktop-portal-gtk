xdg-desktop-portal-gtk (1.0.2-2) unstable; urgency=medium

  * Build-depend on xdg-desktop-portal 1.0.2

 -- Simon McVittie <smcv@debian.org>  Mon, 10 Sep 2018 23:02:32 +0100

xdg-desktop-portal-gtk (1.0.2-1) unstable; urgency=medium

  * New upstream release
  * d/copyright: Update

 -- Simon McVittie <smcv@debian.org>  Mon, 10 Sep 2018 10:55:11 +0100

xdg-desktop-portal-gtk (1.0.1-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.2.1 (no changes required)

 -- Simon McVittie <smcv@debian.org>  Fri, 31 Aug 2018 08:55:22 +0100

xdg-desktop-portal-gtk (1.0-2) unstable; urgency=medium

  * Build-depend on xdg-desktop-portal 1.0

 -- Simon McVittie <smcv@debian.org>  Thu, 23 Aug 2018 14:22:09 +0100

xdg-desktop-portal-gtk (1.0-1) unstable; urgency=medium

  * New upstream release
  * Install upstream NEWS file
  * Standards-Version: 4.2.0 (no further changes)

 -- Simon McVittie <smcv@debian.org>  Thu, 23 Aug 2018 09:58:05 +0100

xdg-desktop-portal-gtk (0.99-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.1.5
  * Install to /usr/libexec

 -- Simon McVittie <smcv@debian.org>  Sat, 28 Jul 2018 13:20:22 +0100

xdg-desktop-portal-gtk (0.11-1) unstable; urgency=medium

  * New upstream release
    - Drop patch, merged upstream
    - d/copyright: Update
  * Add Suggests: evince, for evince-previewer
  * Standards-Version: 4.1.4 (no changes)

 -- Simon McVittie <smcv@debian.org>  Wed, 25 Apr 2018 16:37:48 +0100

xdg-desktop-portal-gtk (0.10-1) unstable; urgency=medium

  * New upstream release
    - Drop patches, merged upstream
    - d/copyright: Update
  * d/p/configure-Add-gio-unix-2.0.pc-dependency.patch:
    Add patch from upstream adding missing gio-unix-2.0 dependency
  * Depend and build-depend on xdg-desktop-portal 0.10

 -- Simon McVittie <smcv@debian.org>  Fri, 16 Feb 2018 12:51:59 +0000

xdg-desktop-portal-gtk (0.9-2) unstable; urgency=medium

  * Standards-Version: 4.1.3 (no changes)
  * Change Vcs-* to point to salsa.debian.org
  * d/p/0.10/: Update to upstream git commit v0.9-6-gab7be06 for bug
    fixes and an updated Swedish translation

 -- Simon McVittie <smcv@debian.org>  Thu, 18 Jan 2018 08:46:51 +0000

xdg-desktop-portal-gtk (0.9-1) unstable; urgency=medium

  * New upstream release
    - Drop all patches, applied upstream

 -- Simon McVittie <smcv@debian.org>  Fri, 24 Nov 2017 10:43:14 +0000

xdg-desktop-portal-gtk (0.7-3) unstable; urgency=medium

  * Set Rules-Requires-Root to no
  * Use https Format URL in d/copyright
  * Standards-Version: 4.1.1 (no further changes)

 -- Simon McVittie <smcv@debian.org>  Sat, 11 Nov 2017 13:43:44 +0000

xdg-desktop-portal-gtk (0.7-2) unstable; urgency=medium

  * Move patches taken from upstream to debian/patches/0.8/ to make it
    obvious when they can be dropped
  * Upload to unstable

 -- Simon McVittie <smcv@debian.org>  Sat, 10 Jun 2017 10:13:02 +0100

xdg-desktop-portal-gtk (0.7-1) experimental; urgency=medium

  * New upstream release
    - Drop patches, no longer needed
  * Drop build-dependency on libflatpak-dev, which was only needed due
    to bugs in xdg-desktop-portal-dev. Build-depend on a fixed version
    of xdg-desktop-portal-dev instead.
  * debian/patches/*: Update app chooser design from upstream git

 -- Simon McVittie <smcv@debian.org>  Fri, 09 Jun 2017 08:28:33 +0100

xdg-desktop-portal-gtk (0.6-1) experimental; urgency=medium

  * New upstream release
    - Apply some post-release bug fixes from upstream
  * Move to debhelper compat level 10
  * Omit unimplemented configure options --enable-docbook-docs,
    --enable-installed-tests
  * Do not explicitly disable quiet Automake output: dh now does this
    by default

 -- Simon McVittie <smcv@debian.org>  Mon, 03 Apr 2017 18:03:41 +0100

xdg-desktop-portal-gtk (0.5-1) unstable; urgency=medium

  * New upstream release
    - Suggests: accountsservice for the new Account interface

 -- Simon McVittie <smcv@debian.org>  Sat, 21 Jan 2017 16:28:44 +0000

xdg-desktop-portal-gtk (0.3-1) unstable; urgency=medium

  * Initial release. (Closes: #831690)

 -- Simon McVittie <smcv@debian.org>  Wed, 21 Sep 2016 22:14:47 +0100
